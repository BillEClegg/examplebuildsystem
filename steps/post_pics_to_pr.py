#!/usr/bin/env python3

import requests
import json
import base64
import os
import datetime
import logging

_moduleLogger = logging.getLogger(__name__)

def _create_header(token):
    #print("%s" % token.strip())
    return {"PRIVATE-TOKEN": "glpat-Z3GpxoYHAHysD_ERuxZf", "Content-Type": "application/json"}


def _read_picture(file_name):
    with open(file_name, 'rb') as text:
        return base64.b64encode(text.read()).decode()


def _post_file(file, file_data, folder, file_name, header, picRepo):
    # put encoded data into json request
    new_file_data = json.dumps({"commit_message": "commit message", "content":file_data, "branch": "master"})

    # post a picture to a repo
    url = 'https://gitlab.com/api/v4/projects/43685714/repository/files/%s' % ((folder + "/" + file_name).replace('/', '%2F'))
    #url = 'https://api.github.com/repos/%s/contents/%s/%s' % (picRepo, folder, file_name)

    #print(url)
    
    #r=requests.post(url, data=new_file_data, headers=header)

    # post a picture to application repo for use in a note
    url = 'https://gitlab.com/api/v4/projects/43685626/uploads'
    fileA = {'file': open("DIFFDIR/" + file, 'rb')}
    r=requests.post(url, headers={"PRIVATE-TOKEN": "glpat-Z3GpxoYHAHysD_ERuxZf"}, verify=True, files = fileA)

    if (r.ok):
        _moduleLogger.info('Response code: %s', r.status_code)
    else:
        _moduleLogger.error('Bad response code: %s', r.status_code)
        _moduleLogger.error('Bad response text: %s', r.text)

    return r.json()['url']


# post a comment on an issue
def _post_comment_to_pr(urlPicPairs, pullRequestInfo, prNumber, header):
    formatString = "### %s:\n ![capture](%s)\n\n"
    #formatString = "### %s: ![capture](https://gitlab.com/BillEClegg/examplepicturerepository/-/blob/master/%s)\n\n"
    body = """Bleep bloop!
LabVIEW Jenkins diff bot here, serving up a hot & fresh VI diff for your Pull Request!
"""
    for pair in urlPicPairs:
        body += formatString % pair


    org, repo, _ = pullRequestInfo.split('/')
    url = "https://gitlab.com/api/v4/projects/43685626/merge_requests/%s/notes" % (prNumber)
    #url = "https://api.github.com/repos/%s/%s/issues/%s/comments" % (org, repo, prNumber)
    data = json.dumps({"body":body})
    r = requests.post(url, data=data, headers=header)
    if (r.ok):
        _moduleLogger.info('Response code: %s', r.status_code)
    else:
        _moduleLogger.error('Bad response code: %s', r.status_code)
        _moduleLogger.error('Bad response text: %s', r.text)

def post_pics_to_pr(token, localPicfileDirectory, pullRequestInfo, prNumber, picRepo):
    header = _create_header(token)
    pics = [f for f in os.listdir(localPicfileDirectory) if f.endswith(".png")]
    folder = ((pullRequestInfo + "/" + datetime.datetime.now().strftime('%Y-%m-%d/%H:%M:%S'))[11:]).replace(' ', '%20', 1)
    picUrls = []
    for pic in pics:
        picData = _read_picture(os.path.join(localPicfileDirectory, pic))
        picUrl = _post_file(pic, picData, folder, os.path.split(pic)[1], header, picRepo)
        picUrls.append((pic, ((picUrl.replace(' ', '%20')).replace(':', '%3A'))))

    if picUrls != []:
        _post_comment_to_pr(picUrls, pullRequestInfo, prNumber, header) 
